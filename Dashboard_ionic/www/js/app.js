// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'controller.Geoloc', 'starter.controllers', 'Service.Authentificate', 'MapApp','controllers.User', 'controller.Driver'])

.run(function ($ionicPlatform, AuthService, $rootScope, $state) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    console.log('INIT: Redirection');

      // Redirection de l'utilisateur suivant s'il est logg� ou non 
    $rootScope.$on("$stateChangeStart", function (event, toState, ToParams, fromState, fromParams) {
        console.log('INIT: Enter the app');
        if (toState.authentificate === true) {
            console.log(AuthService.isLoggedIn())
            if (!AuthService.isLoggedIn()) {
                event.preventDefault();
                $state.go('app.login');
            }
        }
    });
  });
})

 // ================================================================
    //-------------------- Routes----------------------------------------
.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
    })

    //login view
    .state('app.login', {
        url: '/login',
        views: {
            'menuContent': {
                templateUrl: 'templates/login.html',
                controller: 'UserCtrl'
            }
        },
        authentificate: false
    })

  .state('app.missionMap', {
      url: '/map',
      views: {
          'menuContent': {
              templateUrl: 'templates/missionMap.html',
              controller: 'GeoCtrl'
          }
      },
      authentificate: true
  })

  .state('app.driverInfo', {
      url: '/driver/:idDriver',
      views: {
          'menuContent': {
              templateUrl: 'templates/driver.html',
              controller: 'driverCtrl'
          }
      },
      authentificate: true
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
