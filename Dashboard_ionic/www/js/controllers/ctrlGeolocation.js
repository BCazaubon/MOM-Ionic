﻿angular.module('controller.Geoloc',[])

.controller('GeoCtrl', function ($scope, $q, $http, $localStorage, $ionicPopup) {

    /*var posOptions = { timeout: 10000, enableHighAccuracy: false };

    $cordovaGeolocation
      .getCurrentPosition(posOptions)
        console.log("Current position: " + posOptions)
      .then(function (position) {
          this.lat = position.coords.latitude
          this.long = position.coords.longitude
      }, function (err) {
          // error
      });


    var watchOptions = {
        timeout: 3000,
        enableHighAccuracy: false // may cause errors if true
    };

    var watch = $cordovaGeolocation.watchPosition(watchOptions);
    watch.then(
      null,
      function (err) {
          // error
      },
      function (position) {
          this.lat = position.coords.latitude
          this.long = position.coords.longitude
      });

    watch.clearWatch();
    // OR
    $cordovaGeolocation.clearWatch(watch)
      .then(function (result) {
          // success
      }, function (error) {
          // error
      });

    var GetNavPosition = navigator.geolocation.getCurrentPosition(function (position) {
        var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        $scope.positions.push({ lat: pos.k, lng: pos.B });
        console.log(pos);
        $scope.map.setCenter(pos);
        $ionicLoading.hide();
    });

    // ========================= A FINIR! =======================================
    // Geolocalisation



    $scope.SendGeolocalisationData = function (LocData) {
        io.socket.post('/getLocalisation', { token: $localStorage.Token }, function (data, jwres) {
            data.lat = this.lat;
            data.long = this.long;
            console.log(data);
        });
    };*/


  var server_url = 'http://10.33.1.238:1337/api';
  // ================================================================
  console.log("SOCKET : Start")
  getDriver = function () {
        req = {
            method: 'POST',
            url: server_url + '/ListUserForManager',
            headers: {
                'authorization': $localStorage.Token,
            },
            data: {
            }
        }

        var success = function (result) {
            if (result.statusText == "OK") {
                console.log(result);
                $scope.drivers = result.data;
            }
        }

        var error = function (err) {
            $ionicPopup.alert({
                title: err.statusText,
                template: err.data.err
            });
        }

        $http(req).then(success, error);
        console.log("Terminé" + success.name);
    }

    getDriver();

    $scope.refresh = function() {
      getDriver();
    }
});

