angular.module('factory.listeMissionUser', [])
/**
 * The Projects factory handles saving and loading projects
 * from local storage, and also lets us save and load the
 * last active project index.
 */
.factory('MissionUser', function($localStorage, $http, $q) {

	// ================================================================
	// Get Serveur Url
	var server_url = "";

	// ================================================================
    // Save server response in listeMissionUser
    var $localstorage.listeMissionUser = undefined;

	// ================================================================
    //Get toggle status
    getMissionUser = function (driverId){
        req =   {
            method: 'POST',
            url: server_url + '/ChangeIsOpen',
            headers: {
                'authorization': $localStorage.Token,
            },
            data: { 
                id: driverId,
            }
        }
       
        // defer = la promesse, ce qui sera mis dans le defer.resolve/.reject va devenir ce que la promesse affichera
        var defer = $q.defer();

        // connection au serveur pour récupérer les listes des serrures d'un utilisateur
        var success = function(result){
            return defer.resolve(result);
        }
        var error = function(err){
            return defer.reject(err);
        }

        $http(req).then(success,error);
        return defer.promise;
    }

    // ================================================================
    // Activate socket on "mission" tag
    io.socket.on('mission',function(msg){
        console.log(msg);
        switch(msg.verb) {
            case 'updated':
                break;
    })

    // ================================================================
    // Get listeMissionUser
    this.getListeMissionUser = function(){
    	if(listeMissionUser != undefined) {
    		return listeMissionUser;
    	} else {
    		listeMissionUser = getMissionUser();
    		return getMissionUser();
    	}
    }
}