﻿angular.module('mapper.controllers', [])

.controller('MapCtrl', ['$scope', 'GoogleMapsService','$rootScope',function ($scope, GoogleMapsService, $rootScope) {
    // When you first go into your view (with a map) you should call the following method.

        GoogleMapsService.initService(document.getElementById("map"));

    // Set up some example test data for a leg to be stopped over on the route
    $scope.journeyLegs = [];
    var journeyLeg = { "zipPostalCode": "66045", "contactId": "1" };
    var apiKey = "AIzaSyBLo2u8RNegU0iAS2ZATEe68HEaNIbB5rc";
    $scope.journeyLegs.push(journeyLeg);

// The following code can be used to add a route to the map. This particular style of calling the
    // Google service will use ‘waypoints’ for each contact along the route.

    // Call the method to add the route
    console.log("2- Call addroute");
    GoogleMapsService.addRoute(94087, 27215);

    // Call this method to add the route
    $scope.addRoute = function (origin, destination) {
        GoogleMapsService.initService();
        if (origin !== "" && destination !== "") {
            // Callout to Google to get route between first and last contact.
            // The 'legs' between these contact will give us the distance/time
            var waypts = [];
            for (i = 0, len = $scope.journeyLegs.length; i < len; i++) {
                waypts.push({
                    location: $scope.journeyLegs[i].zipPostalCode,
                    stopover: true
                });
            }
            console.log("1- Add route");
            GoogleMapsService.addRoute(origin, destination, waypts, true);
        }
    };


    // Handle callback from Google Maps after route has been generated
    var deregisterUpdateDistance = $rootScope.$on('googleRouteCallbackComplete', function(event, args) {
        $scope.updateDistance();
    });
 
    // Deregister the handler when scope is destroyed
    $scope.$on('$destroy', function() {
        deregisterUpdateDistance();
    });
  
// Points of interest:
 
// 1. The origin and destination parameters are zip/post codes
 
// 2. $scope.journeyLegs is an array of contacts on the route (each contact has a zip/post code)
 
// 3. Once the callout to Google (to get the route) has completed, an event will be broadcast from
 
//      the GoogleMapsService – this event (‘googleRouteCallbackComplete’) can be handled by the
 
//      controller code and the route information interrogated in more detail e.g. to get the distance
 
//      between the contacts legs.

    $scope.updateDistance = function() {
        $timeout(function() {
            // Get the route saved after callback from Google directionsService (to get route)
            var routeResponse = GoogleMapsService.getRouteResponse();
            if (routeResponse) {
                // We’ve only defined one route with waypoints (or legs) along it
                var route = routeResponse.routes[0];
                // The following is an example of getting the distance and duration for the last leg of the route
                var distance = route.legs[route.legs.length-1].distance;
                var duration = route.legs[route.legs.length-1].duration;
                // Add some code to use the above distance/duration data:
                // distance.value and duration.value give you numeric values
                // distance.text and duration.text give you a text version.  E.g. 120 mi
            }
        });
    };

    $scope.removeRoute = function () {
        // Use the following call if you need to remove the route from the map (it won’t destroy the map)
        GoogleMapsService.removeRoute();
    };
}]);