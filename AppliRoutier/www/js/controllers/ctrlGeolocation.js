﻿var Geoloc = angular.module('controller.Geoloc',[]);

Geoloc.controller('GeoCtrl', function ($scope, $q, $http, $localStorage) {

    /*var posOptions = { timeout: 10000, enableHighAccuracy: false };

    $cordovaGeolocation
      .getCurrentPosition(posOptions)
        console.log("Current position: " + posOptions)
      .then(function (position) {
          this.lat = position.coords.latitude
          this.long = position.coords.longitude
      }, function (err) {
          // error
      });


    var watchOptions = {
        timeout: 3000,
        enableHighAccuracy: false // may cause errors if true
    };

    var watch = $cordovaGeolocation.watchPosition(watchOptions);
    watch.then(
      null,
      function (err) {
          // error
      },
      function (position) {
          this.lat = position.coords.latitude
          this.long = position.coords.longitude
      });

    watch.clearWatch();
    // OR
    $cordovaGeolocation.clearWatch(watch)
      .then(function (result) {
          // success
      }, function (error) {
          // error
      });

    var GetNavPosition = navigator.geolocation.getCurrentPosition(function (position) {
        var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        $scope.positions.push({ lat: pos.k, lng: pos.B });
        console.log(pos);
        $scope.map.setCenter(pos);
        $ionicLoading.hide();
    });

    // ========================= A FINIR! =======================================
    // Geolocalisation



    $scope.SendGeolocalisationData = function (LocData) {
        io.socket.post('/getLocalisation', { token: $localStorage.Token }, function (data, jwres) {
            data.lat = this.lat;
            data.long = this.long;
            console.log(data);
        });
    };*/

  // ================================================================
  // Server list
  var server_url = "http://10.33.1.238:1337/api";
  var position = [
    {posx: 44.854390 , posy: -0.566354},
    {posx: 43.759283 , posy: -0.573561},
    {posx: 48.857934 , posy: 2.351331},
    {posx: 45.764641 , posy: 4.836149},
  ];
  var index = 0;

  setDriver = function (posx, posy, idVeh){
      req =   {
          method: 'POST',
          url: server_url + '/getLocalisation',
          headers: {
              'authorization': $localStorage.Token,
          },
          data: {
            vehicule: idVeh,
            long: posx,
            lat: posy,
          }
      }
     
      // defer = la promesse, ce qui sera mis dans le defer.resolve/.reject va devenir ce que la promesse affichera
      var defer = $q.defer();

      // connection au serveur pour récupérer les listes des serrures d'un utilisateur
      var success = function(result){
          return defer.resolve(result);
      }
      var error = function(err){
          return defer.reject(err);
      }

      console.log("test");

      $http(req).then(success,error);
      return defer.promise;
  }

  $scope.ChangeOneDriverPos = function() {
  // choose one driver random et change sa position
    console.log(position[index].posx);
    console.log(position[index].posy);
    console.log($localStorage.User.vehicule);
    setDriver(position[index].posx, position[index].posy, $localStorage.User.vehicule);
    if(index == 3)
    {
      index = 0;
    } else {
      index++;
    }
  }

  $scope.ChangeAllDriverPos = function() {
  // choose one driver random et change sa position
    for (var i = 0; i <= 10; i++) {
      console.log(position[index].posx);
      console.log(position[index].posy);
      console.log($localStorage.User.vehicule);
      setDriver(position[index].posx, position[index].posy, $localStorage.User.vehicule);
      if(index==3)
      {
        index = 0;
      } else {
        index++;
      }
    };
  }
});

