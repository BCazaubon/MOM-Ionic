// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'Service.Authentificate', 'login.controllers', 'MapApp', 'controller.Geoloc', 'controller.ListMissionUser', 'controllers.User'])

.run(function ($ionicPlatform, AuthService, $rootScope, $state) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    console.log('INIT: Redirection');

      // Redirection de l'utilisateur suivant s'il est logg� ou non 
    $rootScope.$on("$stateChangeStart", function (event, toState, ToParams, fromState, fromParams) {
        console.log('INIT: Enter the app');
        if (toState.authentificate === true) {
            console.log(AuthService.isLoggedIn())
            if (!AuthService.isLoggedIn()) {
                event.preventDefault();
                $state.go('app.login');
            }
        }
    });
  });
})

 // ================================================================
    //-------------------- Routes----------------------------------------
.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
    })

    //login view
    .state('app.login', {
        url: '/login',
        views: {
            'menuContent': {
                templateUrl: 'templates/login.html',
                controller: 'UserCtrl'
            }
        },
        authentificate: false
    })

  .state('app.logs', {
    url: '/logs',
    views: {
      'menuContent': {
        templateUrl: 'templates/logs.html'
      }
    },
      authentificate: true
  })

  .state('app.account', {
      url: '/account',
      views: {
        'menuContent': {
          templateUrl: 'templates/account.html'
        }
      },
      authentificate: true
  })

    .state('app.missionsList', {
        url: '/missionsList',
        views: {
          'menuContent': {
            templateUrl: 'templates/missionsList.html',
            controller: 'PlaylistsCtrl'
          }
        },
        authentificate: true
    })

  .state('app.missionDetails', {
    url: '/missionDetails/:playlistId',
    views: {
      'menuContent': {
          templateUrl: 'templates/missionDetails.html',
        controller: 'PlaylistCtrl'
      }
    },
    authentificate: true
  })

  .state('app.missionAccepted', {
      url: '/missionAccepted/:playlistId',
      views: {
          'menuContent': {
              templateUrl: 'templates/missionAccepted.html',
              controller: 'PlaylistCtrl'
          }
      },
      authentificate: true
  })

  .state('app.missionMap', {
      url: '/map',
      views: {
          'menuContent': {
              templateUrl: 'templates/missionMap.html',
          }
      },
      authentificate: true
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
